package com.aswdc.MatrimonyLive.Database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.aswdc.MatrimonyLive.Models.StateModel;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;

public class TblStateNames extends MyDatabase {
    public static final String TBL_NAME="TblStateNames";
    public static final String STATE_ID="StateID";
    public static final String STATE_NAME="StateName";

    ArrayList<StateModel> stateModels;

    public TblStateNames(Context context) {
        super(context);
    }

    public ArrayList<StateModel> getStateModels(){
        SQLiteDatabase db=getReadableDatabase();
        String query="SELECT * FROM "+ TBL_NAME;
        Cursor cursor=db.rawQuery(query,null);
        cursor.moveToFirst();
        for (int i =0; i<cursor.getCount();i++){
            StateModel stateModel=new StateModel();
            stateModel.setStateID(cursor.getInt(cursor.getColumnIndex(STATE_ID)));
            stateModel.setStateName(cursor.getString(cursor.getColumnIndex(STATE_NAME)));
            stateModels.add(stateModel);
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return stateModels;
    }
}
