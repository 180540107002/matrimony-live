package com.aswdc.MatrimonyLive.Database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.aswdc.MatrimonyLive.Models.LanguageModel;

import java.util.ArrayList;

public class TblLanguages extends MyDatabase {
    public static final String TBL_NAME="TblLanguages";
    public static final String LANGUAGE_ID="LanguageID";
    public static final String LANGUAGE="Language";

    ArrayList<LanguageModel> languageModels;
    public TblLanguages(Context context) {
        super(context);
    }

    public ArrayList<LanguageModel> getLanguageModels(){
        SQLiteDatabase db= getReadableDatabase();
        String query= " SELECT * FROM "+ TBL_NAME;
        Cursor cursor=db.rawQuery(query,null);
        cursor.moveToFirst();
        for (int i=0; i < cursor.getCount();i++){
            LanguageModel languageModel=new LanguageModel();
            languageModel.setLanguage(cursor.getString(cursor.getColumnIndex(LANGUAGE)));
            languageModel.setLanguageID(cursor.getInt(cursor.getColumnIndex(LANGUAGE_ID)));
            languageModels.add(languageModel);
            cursor.moveToNext();
        }
        db.close();
        cursor.close();
        return languageModels;
    }
}
