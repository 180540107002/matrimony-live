package com.aswdc.MatrimonyLive.Database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.aswdc.MatrimonyLive.Models.CityModel;

import java.util.ArrayList;

public class TblCityNames extends MyDatabase {
    public static  final String TBL_NAME="TblCityNames";
    public static final String CITY_NAME="CityName";
    public static final String CITY_CODE="CityCode";
    public static final String STATE_CODE="StateCode";

    Context context;
    ArrayList<CityModel> cityModels;
    public TblCityNames(Context context) {
        super(context);
    }

    public ArrayList<CityModel> getCityModels(int stateCode){
        SQLiteDatabase db=getReadableDatabase();
        cityModels=new ArrayList<>();
        String query="SELECT "+CITY_NAME+","+CITY_CODE+" FROM "+ TBL_NAME+" WHERE "+STATE_CODE+" =?";
        Cursor cursor=db.rawQuery(query,new String[]{"" + stateCode});
        cursor.moveToFirst();
        for (int i =0; i<cursor.getCount();i++){
            CityModel cityModel=new CityModel();
            cityModel.setCityName(cursor.getString(cursor.getColumnIndex(CITY_NAME)));
            cityModel.setCityCode(cursor.getInt(cursor.getColumnIndex(CITY_CODE)));
            cityModel.setStateCode(cursor.getInt(cursor.getColumnIndex(STATE_CODE)));
            cityModels.add(cityModel);
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return cityModels;
    }
}
