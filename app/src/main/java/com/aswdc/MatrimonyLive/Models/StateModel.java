package com.aswdc.MatrimonyLive.Models;

import java.io.Serializable;

public class StateModel implements Serializable {
    int StateID;
    String StateName;

    public int getStateID() {
        return StateID;
    }

    public void setStateID(int stateID) {
        StateID = stateID;
    }

    public String getStateName() {
        return StateName;
    }

    public void setStateName(String stateName) {
        StateName = stateName;
    }

    @Override
    public String toString() {
        return "StateModel{" +
                "StateID=" + StateID +
                ", StateName='" + StateName + '\'' +
                '}';
    }
}
