package com.aswdc.MatrimonyLive.Models;

import java.io.Serializable;

public class CityModel implements Serializable {
    String CityName;
    int CityCode;
    int StateCode;

    public String getCityName() {
        return CityName;
    }

    public void setCityName(String cityName) {
        CityName = cityName;
    }

    public int getCityCode() {
        return CityCode;
    }

    @Override
    public String toString() {
        return "CityModel{" +
                "CityName='" + CityName + '\'' +
                ", CityCode=" + CityCode +
                ", StateCode=" + StateCode +
                '}';
    }

    public void setCityCode(int cityCode) {
        CityCode = cityCode;
    }

    public int getStateCode() {
        return StateCode;
    }

    public void setStateCode(int stateCode) {
        StateCode = stateCode;
    }
}
