package com.aswdc.MatrimonyLive.Models;

public class LanguageModel {
    int LanguageID;
    String Language;

    public int getLanguageID() {
        return LanguageID;
    }

    public void setLanguageID(int languageID) {
        LanguageID = languageID;
    }

    public String getLanguage() {
        return Language;
    }

    public void setLanguage(String language) {
        Language = language;
    }

    @Override
    public String toString() {
        return "LanguageModel{" +
                "LanguageID=" + LanguageID +
                ", Language='" + Language + '\'' +
                '}';
    }
}
