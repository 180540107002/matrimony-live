package com.aswdc.MatrimonyLive;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.btnActFavourite)
    Button btnActFavourite;
    @BindView(R.id.btnActRegisterMe)
    Button btnActRegisterMe;
    @BindView(R.id.btnActSearch)
    Button btnActSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btnActFavourite)
    public void onBtnActFavouriteClicked() {

    }

    @OnClick(R.id.btnActRegisterMe)
    public void onBtnActRegisterMeClicked() {
        Intent intent=new Intent(MainActivity.this,RegisterMeActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.btnActSearch)
    public void onBtnActSearchClicked() {
    }
}